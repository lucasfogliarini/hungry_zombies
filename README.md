### Setup

- Install [Node.js](https://nodejs.org/en/download/) or via [package manager](https://nodejs.org/en/download/package-manager/)
- Install [Meteor.js](https://www.meteor.com/install)
- Install [MongoDB Community Edition](https://docs.mongodb.com/manual/administration/install-community/)

### Running the app

```bash
cd /hungry_zombies
meteor npm install
meteor
```
running on [http://localhost:3000](http://localhost:3000)

### Actions & Permissions
 - vote
 - add restaurants
 - remove restaurants  __admin__
 - set poll end time  __admin__
